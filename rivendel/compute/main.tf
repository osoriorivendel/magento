#-----compute/main.tf

data "aws_ami" "server_ami" {
  owners      = ["379101102735"]
  most_recent = true

  #  filter {
  #    name   = "owner-alias"
  #    values = ["amazon"]
  #  }
  #
  #  filter {
  #    name   = "name"
  #    values = ["amzn-ami-hvm*-x86_64-gp2"]
  #  }
  #}

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
    name   = "name"
    values = ["debian-stretch-hvm*"]
  }
}

#data "template_file" "user-init" {
#  count    = 2
#  template = "${file("${path.module}/userdata.tpl")}"

#  vars {
#    firewall_subnets = "${element(var.subnet_ips, count.index)}"
#  }
#}

resource "aws_instance" "tf_server" {
  count         = "${var.instance_count}"
  instance_type = "${var.instance_type}"
  ami           = "${data.aws_ami.server_ami.id}"

  tags {
    Name  = "osorio-terraform-${count.index +1}"
    Owner = "Osorio"
  }

  key_name               = "${aws_key_pair.tf_auth.id}"
  vpc_security_group_ids = ["${var.security_group}"]
  subnet_id              = "${element(var.subnets, count.index)}"

  #user_data              = "${data.template_file.user-init.*.rendered[count.index]}"
}
