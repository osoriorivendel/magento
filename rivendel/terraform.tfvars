aws_region = "us-east-1"
vpc_cidr = "10.123.0.0/16"
public_cidrs = [
    "10.123.1.0/24",
    "10.123.2.0/24"
    ]
accessip = "0.0.0.0/0"
private_cidrs = [
  "10.123.3.0/24",
  "10.123.4.0/24"
]
key_name = "osorio-terraform_key"
public_key_path = "/etc/keys/sshkeys/id_rsa.pub"
server_instance_type = "t2.micro"
instance_count = 1
storage_type = "gp2"
engine = "mysql"
engine_version = "5.7"
instance_class = "db.t2.micro"
username = "magento"
password = "naotemsenha123"
