#-----networking/outputs.tf

output "public_subnets" {
  value = "${aws_subnet.tf_public_subnet.*.id}"
}

output "public_sg" {
  value = "${aws_security_group.tf_public_sg.id}"
}

output "subnet_ips" {
  value = "${aws_subnet.tf_public_subnet.*.cidr_block}"
}

output "private_subnets" {
  value = "${aws_subnet.tf_private_subnet.*.id}"
}

output "private_sg" {
  value = "${aws_security_group.tf_private_sg.id}"
}

output "aws_db_subnet_group_teste" {
  value = "${aws_db_subnet_group.padrao.id}"
}

#output "aws_db_security_group" {
#  value = "${aws_security_group.tf_private_sg.id}"
#}

output "aws_db_security_group" {
  value = "${aws_security_group.tf_private_sg.*.id}"
}
