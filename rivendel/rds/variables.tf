#-------------variables-rds

variable "allocated_storage" {
  default = "20"
}

variable "storage_type" {}

variable "engine" {}

variable "engine_version" {}

variable "instance_class" {}

variable "username" {}

variable "password" {}

variable "db_subnet_group_name" {}

variable "vpc_security_group_ids" {
  type = "list"
}
