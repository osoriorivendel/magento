provider "aws" {
  region = "${var.aws_region}"
  assume_role {
  role_arn     = "arn:aws:iam::485164690107:role/rivendel-servicos"
  }

}

# Deploy Networking Resources

module "networking" {
  source        = "./networking"
  vpc_cidr      = "${var.vpc_cidr}"
  public_cidrs  = "${var.public_cidrs}"
  accessip      = "${var.accessip}"
  private_cidrs = "${var.private_cidrs}"
}

# Deploy Compute Resources

module "compute" {
  source          = "./compute"
  instance_count  = "${var.instance_count}"
  key_name        = "${var.key_name}"
  public_key_path = "${var.public_key_path}"
  instance_type   = "${var.server_instance_type}"
  subnets         = "${module.networking.public_subnets}"
  security_group  = "${module.networking.public_sg}"
  subnet_ips      = "${module.networking.subnet_ips}"
}

# Deploy RDS

module "rds" {
  source                 = "./rds"
  storage_type           = "${var.storage_type}"
  engine                 = "${var.engine}"
  engine_version         = "${var.engine_version}"
  instance_class         = "${var.instance_class}"
  username               = "${var.username}"
  password               = "${var.password}"
  db_subnet_group_name   = "${module.networking.aws_db_subnet_group_teste}"
  vpc_security_group_ids = "${module.networking.aws_db_security_group}"
}

# Deploy Route 53

module "route53" {
  source = "./route53"
  ip_ec2 = "${module.compute.server_ip}"
}

resource "local_file" "saida" {
  content  = "${module.compute.server_ip} ansible_user=admin ansible_become=true"
  filename = "/etc/ansible/hosts"
}

resource "local_file" "var1" {
  content  = "endpoint:  ${module.rds.endpoint}"
  filename = "/etc/ansible/roles/magento/tasks/var1.yml"
}

resource "local_file" "var2" {
  content  = "username_db:  ${var.username}"
  filename = "/etc/ansible/roles/magento/tasks/var2.yml"
}

resource "local_file" "var3" {
  content  = "password_db:  ${var.password}"
  filename = "/etc/ansible/roles/magento/tasks/var3.yml"
}
