#--------------main.tf  route53

data "aws_route53_zone" "osorio" {
  name = "osoriohenrique.com.br."
}

resource "aws_route53_record" "www_magento" {
  zone_id = "${data.aws_route53_zone.osorio.zone_id}"
  name    = "www.magento.${data.aws_route53_zone.osorio.name}"
  type    = "A"
  ttl     = "300"
  records = ["${var.ip_ec2}"]
}

#resource "aws_route53_record" "www" {
#  zone_id = "Z2AHLFJXQ8APC4"
#  name    = "www.magento.osoriohenrique.com.br."
#  type    = "A"
#  ttl     = "300"
#  records = ["${var.ip_ec2}"]
#}

