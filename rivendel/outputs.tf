#----root/outputs.tf-----

#---Networking Outputs -----

output "Public Subnets" {
  value = "${join(", ", module.networking.public_subnets)}"
}

output "Subnet IPs" {
  value = "${join(", ", module.networking.subnet_ips)}"
}

output "Private Subnets" {
  value = "${join(", ", module.networking.private_subnets)}"
}

output "Public Security Group" {
  value = "${module.networking.public_sg}"
}

output "Private Security Group" {
  value = "${module.networking.private_sg}"
}

#---Compute Outputs ------

output "Public Instance IDs" {
  value = "${module.compute.server_id}"
}

output "Public Instance IPs" {
  value = "${module.compute.server_ip}"
}

output "RDS Endpoint" {
  value = "${module.rds.endpoint}"
}

#--------- Route 53 outputs

output "Dominio para acesso http" {
  value = "${module.route53.access_domain}"
}
