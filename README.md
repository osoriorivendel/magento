######IMPORTANTE#########
- Para correta execução, mova o diretório magento para "/etc/ansible/roles/".

"mv magento /etc/ansible/roles/"

- Crie o caminho padrão para criação de chaves:

"mkdir -p /etc/keys/sshkeys/"

- Crie as chaves publicas da seguinte forma:

"ssh-keygen -f /etc/keys/sshkeys/id_rsa"

OBS: NAO ALTERAR O NOME DA CHAVE

- Alterar o campo "private_key_file" para /etc/keys/sshkeys/id_rsa

######## Lembretes ##########

O usuário que executar o terraform precisa ter permissão recursiva de escrita/leitura no diretório "/etc/ansible/roles/magento" e também em "/etc/keys/sshkeys/".

######## Execução #############

1 - Acessar o diretorio rivendel e executar os seguintes comandos:

- "terraform init"
- "terraform apply"

2 - Acessar o diretório /etc/ansible/roles/magento/tasks e executar o seguinte comando:

- "ansible-playbook main.yml"

########### Acesso ao site #############

Ao finalizar o comando terraform apply, será exibido o output "Dominio para acesso http" copie a saída e acesse pelo navegador.
